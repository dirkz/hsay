{-# LANGUAGE OverloadedStrings #-}

module HSay
  ( sayToDirectory
  )
where

import qualified Data.Text                     as T
import           Prelude                 hiding ( FilePath )
import           Turtle

-- |Outputs the given text to a aiff-file in the given directory,
-- |using the macsOS command-line program `say`,
-- |in the given `voice`, and returns the anki ref to it as `Text`,
-- |or an error message.
-- |You can fake the whole operation by setting `executeForReal` to False.
-- |TODO: Include the voice in the filename.
sayToDirectory
  :: MonadIO io => Bool -> Text -> FilePath -> Text -> io (Either Text Text)
sayToDirectory executeForReal voice ankiMediaDir theText = do
  let aiff       = aiffFileNameFromPhrase theText
  let targetAiff = ankiMediaDir </> aiff
  result <- if executeForReal
    then say voice theText targetAiff
    else return $ Right ()
  case result of
    Right _         -> return $ Right $ ankiRef aiff
    Left  errorText -> return $ Left errorText

-- |Invokes `say` to get a spoken representation of a text to the given filename,
-- |in the given voice.
say :: MonadIO io => Text -> Text -> FilePath -> io (Either Text ())
say voice sayWhat outFileName = do
  (exitCode, out, err) <- procStrictWithErr
    "say"
    ["-v", voice, sayWhat, "-o", format fp outFileName]
    empty
  case exitCode of
    ExitSuccess   -> return $ Right ()
    ExitFailure _ -> return $ Left $ out <> err

-- |Makes an Anki-suitable sound ref from a filename, for pasting
ankiRef fileName = "[sound:" <> format fp fileName <> fromString "]"

-- |Remove selected non-chars from a text
removeNonFileNameChars = T.map conv
 where
  conv ':'  = ' '
  conv ','  = ' '
  conv '.'  = ' '
  conv '?'  = ' '
  conv '!'  = ' '
  conv '-'  = ' '
  conv '"'  = ' '
  conv '\'' = ' '
  conv '—'  = ' '
  conv '–'  = ' '
  conv '('  = ' '
  conv ')'  = ' '
  conv '…'  = ' '
  conv '/'  = ' '
  conv '['  = ' '
  conv ']'  = ' '
  conv '№' = ' '
  conv '¿' = ' '
  conv ch   = ch

-- |Replace all WS (whitespace) with 'ch'
replaceWhiteSpace ch = T.map conv
  where conv theCh = if theCh == ' ' then ch else theCh

-- |Reduces groups of more than 1 whitespace to just 1
removeExtranousWhiteSpace = T.unwords . map T.strip . T.words

-- |Converts a phrase into a filename.
baseFileNameFromPhrase :: Text -> FilePath
baseFileNameFromPhrase = fileName
 where
  fileName =
    fromString
      . T.unpack
      . replaceWhiteSpace '-'
      . removeExtranousWhiteSpace
      . removeNonFileNameChars

-- |Uses 'baseFileNameFromPhrase', then attaches ".aiff"
aiffFileNameFromPhrase :: Text -> FilePath
aiffFileNameFromPhrase theText =
  baseFileNameFromPhrase theText <.> fromString "aiff"
