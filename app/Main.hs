{-# LANGUAGE OverloadedStrings #-}

module Main where

import           Turtle
import           Data.Maybe

import           HSay

defaultVoice = "Milena"

parser :: Parser (Maybe Text, Text)
parser =
  (,)
    <$> optional
          (optText "voice"
                   'v'
                   "The voice to use for speaking (default: Milena [Russian])"
          )
    <*> argText "phrase" "The phrase to speak"

start ankiMediaFP = do
  (optVoice, phrase) <- options
    "Utility for generating speech aiffs from phrases, and copying them to the directory ANKI_MEDIA_DIR"
    parser
  let voice = fromMaybe defaultVoice optVoice
  sayToDirectory True voice ankiMediaFP phrase

main :: IO ()
main = do
  ankiMediaDir <- need "ANKI_MEDIA_DIR"
  case fromText <$> ankiMediaDir of
    Nothing          -> die "Please set ANKI_MEDIA_DIR."
    Just ankiMediaFP -> do
      result <- start ankiMediaFP
      case result of
        Left  err    -> die err
        Right string -> printf ("" % s % "\n") string
